defmodule PipoBiboWeb.PageController do
  use PipoBiboWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
