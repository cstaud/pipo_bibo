FROM elixir:1.9-alpine

RUN apk --update --no-cache --virtual add \
    bash \
    inotify-tools \
    npm

RUN mix local.hex --force && \
    mix local.rebar --force

WORKDIR /opt/app